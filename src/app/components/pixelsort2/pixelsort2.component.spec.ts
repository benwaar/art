import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pixelsort2Component } from './pixelsort2.component';

describe('Pixelsort2Component', () => {
  let component: Pixelsort2Component;
  let fixture: ComponentFixture<Pixelsort2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pixelsort2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pixelsort2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
