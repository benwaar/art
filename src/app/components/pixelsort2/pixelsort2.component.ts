import { Component, OnInit } from '@angular/core';
import * as p5js from 'p5';
import { getPixelColor, setPixelColor, Brightness } from '../pixellib';

@Component({
  selector: 'app-pixelsort2',
  templateUrl: '../p5xjstemplate.html',
  styleUrls: ['./pixelsort2.component.scss']
})
export class Pixelsort2Component implements OnInit {
  title = 'pixelsort 2';
  source = 'https://gitlab.com/benwaar/art/-/blob/master/src/app/components/pixelsort2/pixelsort2.component.ts';
  private p5;

  ngOnInit(): void {
    this.p5 = new p5js(this.sketch);
  }

  private sketch(p: any) {
    let img;

    p.preload = () => {
      img = p.loadImage('./assets/shorepart3.png');
    };

    p.setup = () => {
      p.createCanvas(320, 320).parent('canvasContainer');
      p.noLoop();
      p.colorMode(p.RGB);
      p.background(p.GRAY);
      img.loadPixels();
      p.image(img, 0, 0);
      p.loadPixels();
    };

    p.draw = () => {
      let colora: p5js.Color;
      let colorb: p5js.Color;

      for (let pass = 0; pass < 3; pass++) {
        for (let c = 0; c < p.width; c++) {
          for (let r = 0; r < p.height; r++) {
            if (c !== p.width - 1 && r !== p.height - 1) {
              const tx = p.round(p.random(c, p.width));
              const ty = p.round(p.random(r, p.height));
              colora = getPixelColor(p, c, r);
              colorb = getPixelColor(p, tx, ty);
              if (Brightness(p, colorb) > Brightness(p, colora)) {
                setPixelColor(p, c, r, colorb);
                setPixelColor(p, tx, ty, colora);
              }
            }
          }
          p.updatePixels();
        }
      }
      console.log('complete');
    };
  }
}
