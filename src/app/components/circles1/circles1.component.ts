import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-circles1',
  templateUrl: './circles1.component.html',
  styleUrls: ['./circles1.component.scss']
})
export class Circles1Component implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;
  private size = 320;
  private dpr = window.devicePixelRatio;
  private circles = [];

  private minRadius = 2;
  private maxRadius = 77;
  private totalCircles = 999;
  private createCircleAttempts = 999;

  constructor() { }

  private doesCircleHaveACollision(circle: { radius: any; x: any; y: any; }) {

    for (const otherCircle of this.circles) {
      const a = circle.radius + otherCircle.radius;
      const x = circle.x - otherCircle.x;
      const y = circle.y - otherCircle.y;

      if (a >= Math.sqrt((x * x) + (y * y))) {
        return true;
      }
    }
    return false;
  }

  private createAndDrawCircle() {
    let newCircle: { radius: any; x: any; y: any; };
    let circleSafeToDraw = false;
    for (let tries = 0; tries < this.createCircleAttempts; tries++) {
      newCircle = {
        x: Math.floor(Math.random() * this.size),
        y: Math.floor(Math.random() * this.size),
        radius: this.minRadius
      };

      if (this.doesCircleHaveACollision(newCircle)) {
        continue;
      } else {
        circleSafeToDraw = true;
        break;
      }
    }

    if (!circleSafeToDraw) {
      return;
    }

    for (let radiusSize = this.minRadius; radiusSize < this.maxRadius; radiusSize++) {
      newCircle.radius = radiusSize;
      if (this.doesCircleHaveACollision(newCircle)) {
        newCircle.radius--;
        break;
      }
    }

    this.circles.push(newCircle);
    this.ctx.beginPath();
    this.ctx.arc(newCircle.x, newCircle.y, newCircle.radius, 0, 2 * Math.PI);
    this.ctx.stroke();
  }

  /**
   * Circle Packing based on https://generativeartistry.com/tutorials/circle-packing/
   */
  ngOnInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.canvas.nativeElement.width = this.size * this.dpr;
    this.canvas.nativeElement.height = this.size * this.dpr;
    this.ctx.scale(this.dpr, this.dpr);
    for (let i = 0; i < this.totalCircles; i++) {
      this.createAndDrawCircle();
    }
  }

}
