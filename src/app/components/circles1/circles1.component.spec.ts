import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Circles1Component } from './circles1.component';

describe('Circles1Component', () => {
  let component: Circles1Component;
  let fixture: ComponentFixture<Circles1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Circles1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Circles1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
