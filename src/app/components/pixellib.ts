import * as p5js from 'p5';

/** Returns the color from the last cell of the pixel.
 *  All cells of the pixel will be the same after loading an image.
 *  The number of cells that make up a pixel depends on the screens density.
 */
export function getPixelColor(p: any, x, y) {
    const color: p5js.Color = p.color(0, 0, 0);
    const density = p.pixelDensity();
    const index = 4 * ((y * density) * p.width * density + (x * density));
    color.setRed(p.pixels[index]);
    color.setGreen(p.pixels[index + 1]);
    color.setBlue(p.pixels[index + 2]);
    color.setAlpha(p.pixels[index + 3]);
    return color;
}

/** Sets the color of all cells of a pixel.
 *  The number of cells that make up a pixel depends on the screens density.
 */
export function setPixelColor(p: any, x, y, color: p5js.Color) {
    const density = p.pixelDensity();
    for (let i = 0; i < density; i++) {
        for (let j = 0; j < density; j++) {
            const index = 4 * ((y * density + j) * p.width * density + (x * density + i));
            p.pixels[index] = p.red(color);
            p.pixels[index + 1] = p.green(color);
            p.pixels[index + 2] = p.blue(color);
            p.pixels[index + 3] = p.alpha(color);
        }
    }
}

export function Brightness(p: any, color: p5js.Color) {
    //Refer to https://www.nbdtech.com/Blog/archive/2008/04/27/Calculating-the-Perceived-Brightness-of-a-Color.aspx
    return Math.round(Math.sqrt(
        p.red(color) * p.red(color) * .241 +
        p.green(color) * p.green(color) * .691 +
        p.blue(color) * p.blue(color) * .068));
}
