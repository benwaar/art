import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pixelsort1Component } from './pixelsort1.component';

describe('Pixelsort1Component', () => {
  let component: Pixelsort1Component;
  let fixture: ComponentFixture<Pixelsort1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pixelsort1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pixelsort1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
