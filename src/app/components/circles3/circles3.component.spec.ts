import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Circles3Component } from './circles3.component';

describe('Circles3Component', () => {
  let component: Circles3Component;
  let fixture: ComponentFixture<Circles3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Circles3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Circles3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
