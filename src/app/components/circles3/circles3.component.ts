import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import * as p5 from 'p5';

@Component({
  selector: 'app-circles3',
  templateUrl: '../p5xjstemplate.html',
  styleUrls: ['./circles3.component.scss']
})
export class Circles3Component implements OnInit {
  title = 'circles 3';
  source = 'https://gitlab.com/benwaar/art/-/blob/master/src/app/components/circles3/circles3.component.ts';
  private p5;

  constructor() { }

  ngOnInit(): void {
    this.p5 = new p5(this.sketch);
  }

  private sketch(p: any) {

    p.setup = () => {
      p.createCanvas(320, 320).parent('canvasContainer');
      p.noLoop();
      p.noFill();
    };

    p.drawcircles = () => {
      for (let i = 50; i < 610; i += 18) {
        p.ellipse(0, 0, i);
      }
    };

    p.draw = () => {
      p.background(255);
      p.translate(99, 99);
      p.strokeWeight(2);
      p.drawcircles();
      p.translate(9, 9);
      p.scale(.9);
      p.drawcircles();
    };
  }

}
