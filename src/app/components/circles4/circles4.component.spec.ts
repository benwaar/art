import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Circles4Component } from './circles4.component';

describe('Circles4Component', () => {
  let component: Circles4Component;
  let fixture: ComponentFixture<Circles4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Circles4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Circles4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
