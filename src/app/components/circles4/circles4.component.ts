import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import * as p5 from 'p5';

@Component({
  selector: 'app-circles4',
  templateUrl: '../p5xjstemplate.html',
  styleUrls: ['./circles4.component.scss']
})
export class Circles4Component implements OnInit {
  title = 'circles 4';
  source = 'https://gitlab.com/benwaar/art/-/blob/master/src/app/components/circles4/circles4.component.ts';
  private p5;

  constructor() { }

  ngOnInit(): void {
    this.p5 = new p5(this.sketch);
  }

  private sketch(p: any) {

    p.setup = () => {
      p.createCanvas(320, 320).parent('canvasContainer');
      p.noLoop();
      p.noFill();
    };

    p.drawcircles = () => {
      for (let i = 640; i < 1060; i += 22) {
        p.ellipse(0, 0, i);
      }
    };

    p.draw = () => {
      p.background(255);
      p.translate(-188, 90);
      p.strokeWeight(2);
      p.drawcircles();
      p.translate(8, 9);
      p.strokeWeight(4);
      p.scale(.9);
      p.drawcircles();
    };
  }

}
