import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Circles2Component } from './circles2.component';

describe('Circles2Component', () => {
  let component: Circles2Component;
  let fixture: ComponentFixture<Circles2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Circles2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Circles2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
