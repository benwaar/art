import { Component, OnInit } from '@angular/core';
import * as p5 from 'p5';
const colorMode = (p5 as any).colorMode;

@Component({
  selector: 'app-circles2',
  templateUrl: '../p5xjstemplate.html',
  styleUrls: ['./circles2.component.scss']
})
export class Circles2Component implements OnInit {
  title = 'circles 2';
  source = 'https://gitlab.com/benwaar/art/-/blob/master/src/app/components/circles2/circles2.component.ts';
  private p5;

  constructor() { }

  ngOnInit(): void {
    this.p5 = new p5(this.sketch);
  }

  private sketch(p: any) {

    p.setup = () => {
      p.createCanvas(320, 320).parent('canvasContainer');
      p.noLoop();
    };

    p.draw = () => {

      const tileCountY = 5;
      const tileCountX = 5;
      const tileWidth = 64;
      const tileHeight = 64;
      const diameter = 64;
      const layers = 6;
      const cutoff = 3;

      p.background(204);
      p.noStroke();

      for (let gridY = 0; gridY <= tileCountY; gridY++) {
        for (let gridX = 0; gridX <= tileCountX; gridX++) {

          const posX = tileWidth * gridX;
          const posY = tileHeight * gridY;

          for (let i = 0; i < layers; i++) {
            p.fill(255 - i * layers);
            p.ellipse(posX + i, posY + i, diameter - i * layers, diameter - i * layers);

            if (i >= cutoff) {
              const r = Math.floor(Math.random() * Math.floor(100));
              if (r > 70) {
                i = layers;
              }
            }

          }
        }
      }

    };
  }
}
