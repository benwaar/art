import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pixelsort3Component } from './pixelsort3.component';

describe('Pixelsort3Component', () => {
  let component: Pixelsort3Component;
  let fixture: ComponentFixture<Pixelsort3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pixelsort3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pixelsort3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
