import { Component, OnInit } from '@angular/core';
import * as p5js from 'p5';
import { getPixelColor, setPixelColor, Brightness } from '../pixellib';

@Component({
  selector: 'app-pixelsort3',
  templateUrl: '../p5xjstemplate.html',
  styleUrls: ['./pixelsort3.component.scss']
})
export class Pixelsort3Component implements OnInit {
  title = 'pixelsort 3';
  source = 'https://gitlab.com/benwaar/art/-/blob/master/src/app/components/pixelsort3/pixelsort3.component.ts';
  private p5;

  ngOnInit(): void {
    this.p5 = new p5js(this.sketch);
  }

  private sketch(p: any) {
    let img;

    p.preload = () => {
      img = p.loadImage('./assets/watarun.jpg');
    };

    p.setup = () => {
      p.createCanvas(320, 320).parent('canvasContainer');
      p.noLoop();
      p.colorMode(p.RGB);
      p.background(p.GRAY);
      img.loadPixels();
      p.image(img, 0, 0);
      p.loadPixels();
    };

    class Pixel {
      x: number;
      y: number;
      processed: boolean;
      public constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
        this.processed = false;
      }
    }

    let pixels: Array<Pixel> = new Array();

    function initPixels() {
      let count = 0;
      for (let c = 0; c < p.width; c++) {
        for (let r = 0; r < p.height; r++) {
          const pixel: Pixel = new Pixel(c, r);
          pixels[count] = pixel;
          count++;
        }
      }
    }

    function pixelProcessed(x: number, y: number) {
      pixels[x * (p.width - 1) + y].processed = true;
    }

    function isPixelProcessed(x: number, y: number) {
      if (pixels[x * (p.width - 1) + y].processed === true) {
        return true;
      } else {
        return false;
      }
    }

    function randomPixelNotProcessed() {
      let pixel: Pixel;
      pixel = new Pixel(p.round(p.random(0, p.width - 1)), p.round(p.random(0, p.height - 1)));
      if (isPixelProcessed(pixel.x, pixel.y) === true) {
        pixel = null;
      }
      return pixel;
    }

    function randomPixel(tries: number) {
      let pixel: Pixel = null;
      for (let c = 0; c < tries; c++) {
        const pixelAttempt: Pixel = randomPixelNotProcessed();
        if (pixelAttempt != null) {
          pixel = pixelAttempt;
          c = tries;
        }
      }
      return pixel;
    }

    p.draw = () => {

      initPixels();

      let colora: p5js.Color;
      let colorb: p5js.Color;
      let norands = 0;

      for (let c = 0; c < p.width; c++) {
        for (let r = 0; r < p.height; r++) {
          if (c !== p.width - 1 && r !== p.height - 1) {
            const pixel: Pixel = randomPixel(9);
            if (pixel != null) {
              colora = getPixelColor(p, c, r);
              colorb = getPixelColor(p, pixel.x, pixel.y);
              if (Brightness(p, colorb) < Brightness(p, colora)) {
                setPixelColor(p, c, r, colorb);
                setPixelColor(p, pixel.x, pixel.y, colora);
                pixelProcessed(pixel.x, pixel.y);
              }
            } else {
              norands++;
            }
            pixelProcessed(c, r);
          }
        }
        p.updatePixels();
      }
      //console.log('norands ' + norands);
      console.log('complete');
    };
  }
}
