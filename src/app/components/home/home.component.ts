import { Component, OnInit } from '@angular/core';
import { ArtdataService } from '../../artdata.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  artworks;

  constructor(public artdataService: ArtdataService) { }

  ngOnInit(): void {
    this.artworks = this.artdataService.getArtworks();
  }

}
