import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArtdataService {

  artwork = [
    { name: 'circles 3', route: '/circles3', thumbnail: '/assets/thumbnails/circles3.png' },
    { name: 'pixelsort 3', route: '/pixelsort3', thumbnail: '/assets/thumbnails/pixelsort3.png' },
    { name: 'circles 2', route: '/circles2', thumbnail: '/assets/thumbnails/circles2.png' },
    { name: 'pixelsort 2', route: '/pixelsort2', thumbnail: '/assets/thumbnails/pixelsort2.png' },
    { name: 'circles 4', route: '/circles4', thumbnail: '/assets/thumbnails/circles4.png' },
    { name: 'pixelsort 1', route: '/pixelsort1', thumbnail: '/assets/thumbnails/pixelsort1.png' },
    { name: 'circles 1', route: '/circles1', thumbnail: '/assets/thumbnails/circles1.png' }
  ];

  constructor() { }

  public getArtworks(): Array<{ name, route, thumbnail }> {
    return this.artwork;
  }
}
