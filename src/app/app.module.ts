import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { Circles1Component } from './components/circles1/circles1.component';
import { Circles2Component } from './components/circles2/circles2.component';
import { Circles3Component } from './components/circles3/circles3.component';
import { Circles4Component } from './components/circles4/circles4.component';
import { Pixelsort1Component } from './components/pixelsort1/pixelsort1.component';
import { Pixelsort2Component } from './components/pixelsort2/pixelsort2.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { Pixelsort3Component } from './components/pixelsort3/pixelsort3.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    Circles1Component,
    Circles2Component,
    Circles3Component,
    Circles4Component,
    Pixelsort1Component,
    Pixelsort2Component,
    Pixelsort3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
