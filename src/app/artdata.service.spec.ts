import { TestBed } from '@angular/core/testing';

import { ArtdataService } from './artdata.service';

describe('ArtdataService', () => {
  let service: ArtdataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArtdataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
