import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { Circles1Component } from './components/circles1/circles1.component';
import { Circles2Component } from './components/circles2/circles2.component';
import { Circles3Component } from './components/circles3/circles3.component';
import { Circles4Component } from './components/circles4/circles4.component';
import { Pixelsort1Component } from './components/pixelsort1/pixelsort1.component';
import { Pixelsort2Component } from './components/pixelsort2/pixelsort2.component';
import { Pixelsort3Component } from './components/pixelsort3/pixelsort3.component';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'circles1', component: Circles1Component },
  { path: 'circles2', component: Circles2Component },
  { path: 'circles3', component: Circles3Component },
  { path: 'circles4', component: Circles4Component },
  { path: 'pixelsort1', component: Pixelsort1Component },
  { path: 'pixelsort2', component: Pixelsort2Component },
  { path: 'pixelsort3', component: Pixelsort3Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
