# Generative Art Sketches

* https://benwaar-art.web.app/

## Useful Resources for Generative Art in the browser

* https://generativeartistry.com/tutorials/
* https://p5js.org/
* http://www.generative-gestaltung.de/
* https://natureofcode.com/

## Install locally

Requires Git and Node.

sudo apt update
sudo apt install git-all

git config --global user.email "Your email address"
git config --global user.name "You"

Use nvm to install the right version of node for the angular version
See https://gist.github.com/LayZeeDK/c822cc812f75bb07b7c55d07ba2719b3

sudo apt-get remove nodejs
sudo apt-get purge nodejs

sudo apt install curl
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

Logout and log back in again

nvm install 12.16.2
nvm alias default 12.16.2
nvm use 12.16.2

Using Git clone the repository to have a local copy, then from within the project directory run Run `npm install` to get the dependencies.

Sometimes the Selenuim driver and Protactor versions are out of step so the e2e test fail,
navigate to /node_modules/webdriver-manager/bin and run webdriver-manager update

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
